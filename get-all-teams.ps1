# Get All Teams Powershell Script
#
# author: Chuck Claunch <charles.a.claunch@nasa.gov>
#
# dependencies:
#   Install-Module MicrosoftTeams
#   Install-Module AzureAD
#
# usage (note you should get a login popup):
#   Connect-MicrosoftTeams
#   Connect-AzureAD
#   .\get-all-teams.ps1

Write-Host "Getting list of teams (if this hangs press enter)"

# gets all the teams and stores output (note teams you're not a member of will error out, that's ok)
# note this command may take a while
$teams = Get-Team

# an array of objects to store the teams
$teams_dict = @()

# keep count of teams so we know the script is running
$team_count = 0

foreach ($team in $teams)
{
    # if you're a member of the team, you'll get back a Teams object, so use that
    if ($team.GetType() -Eq [Microsoft.TeamsCmdlets.PowerShell.Custom.Model.TeamSettings])
    {
        $description = ""

        # protection for if the Description object is null
        if ($team.Description)
        {
            # replacing newlines because they'll foul up the csv file
            $description = $team.Description.replace("`n","<cr><lf>")
        }

        # create a new object to store the team data
        $team_dict = New-Object PSObject -property @{GroupId=$team.GroupId; Name=$team.DisplayName; Description=$description}

        # add the new team object to the main array
        $teams_dict += $team_dict
    }
    # if you're not a member of the team, you'll get an error and we'll use AzureAD with
    # the groupid to get the name/description of the team
    else
    {
        # use AzureAD to get the basic name/description of teams we're not a member of
        # note that this gets run for every team you're not a member of so it will take a while
        try
        {
            $az_team = Get-AzureADGroup -ObjectId $team.Substring(51, 36)
        }
        catch
        {
            Write-Host "Error on groupId: "$team.Substring(51, 36)
            Write-Host $_
            continue
        }

        # do a "deep copy" of the string (AzureAD whines if you're doing operations on its object directly)
        $description = Out-String -InputObject $az_team.Description

        # protection for if the Description object is null
        if ($az_team.Description)
        {
            # replacing newlines because they'll foul up the csv file
            $description = $description.replace("`n"," ")
        }

        # create a new object to store the team data
        $team_dict = New-Object PSObject -property @{GroupId=$team.Substring(51,36); Name=$az_team.DisplayName; Description=$description}

        # add the new team object to the main array
        $teams_dict += $team_dict
    }

    $team_count++
    Write-Progress -Activity "Finding teams" -status "Count $team_count"
}

Write-Host "$team_count teams found"

$out_file = "all_teams$(get-date -f yyyyMMdd_HHmmss).csv"

# sort the list by name, order the columns, and export to csv
$teams_dict | Sort-Object -Property Name | Select-Object Name, Description, GroupId | Export-Csv -Path $out_file -NoTypeInformation
